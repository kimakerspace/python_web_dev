# python_web_dev

Web development mit Python und Flask

## Getting started

First, create an virtualenviroment, activate it and install flask in it
```bash
python3 -m venv env 
source env/bin/activate
pip install flask
```