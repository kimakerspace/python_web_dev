from flask import Flask

my_calculator = Flask(__name__)


@my_calculator.route('/')
def hello_world():
    return """
    Welcome to my calculator!
    Usage:
    /add/int:a/int:b
    """


@my_calculator.route('/add/<a>/<b>')
def add(a, b):
    return str(int(a) + int(b))


if __name__ == '__main__':
    my_calculator.run(debug=True)