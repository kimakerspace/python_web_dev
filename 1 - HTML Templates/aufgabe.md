# HTML Cheat-Sheet
https://www.freecodecamp.org/news/html-cheat-sheet-html-elements-list-reference/

# Aufgaben

- Erstelle eine .html Datei, in der du dich vorstellst Diese sollte folgendes beinhalten:
    - Eine Überschrift
    - Einen Titel (für den Tab im Browser)
    - Ein Bild
    - Einen Lebenslauf
    - Ein Skillset mit allen deinen Skills
    - Einen Link (wofür auch immer)

## Jinja2 render templates
Flask unterstützt sogenannte Jinja2 Templates. In diese kannst du kleine Python-Scripte und Variablen mit einbauen. Die Dokumentation findest du hier: https://jinja.palletsprojects.com/en/3.1.x/templates/

Baue deine Seite so mit Jinja2 um, dass du in einem Python-Script deine Skills in einer Liste speichern kannst, und diese dann in das HTML injected werden.
