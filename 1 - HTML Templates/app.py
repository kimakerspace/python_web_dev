from flask import Flask, render_template

app = Flask(__name__)


@app.route('/')
def hello_world():
    return render_template('hello_world.html')

@app.route('/hello/<name>')
def hello_name(name):
    return render_template('hello_name.html', name=name)


if __name__ == '__main__':
    app.run(debug=True)