from flask import Flask, render_template, request, redirect, url_for

app = Flask(__name__)

todos = [
    {'title': 'Learn Python', 'liked': False},
    {'title': 'Learn Flask', 'liked': False},
    {'title': 'Learn SQL', 'liked': False},
]


@app.route('/')
def index():
    return render_template('index.html', todos=todos)


@app.route('/add')
def add():
    if 'todo' in request.args and request.args['todo']:
        todos.append(request.args['todo'])
    return redirect(url_for('index'))


@app.route('/remove')
def remove():
    if 'id' in request.args and request.args['id']:
        todos.pop(int(request.args['id']))
    return redirect(url_for('index'))


@app.route('/like')
def like():
    if 'id' in request.args and request.args['id']:
        todos[int(request.args['id'])]['liked'] = not todos[int(request.args['id'])]['liked']
    return redirect(url_for('index'))


if __name__ == '__main__':
    app.run(debug=True)