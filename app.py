from flask import Flask

app = Flask(__name__)

@app.route("/")
def hello_world():
    return "Hallo, Welt!"

@app.route("/hallo/<name>/")
def hello(name):
    return "Hallo " + name + "!"


app.run(debug=True)